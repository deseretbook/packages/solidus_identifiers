# frozen_string_literal: true

FactoryBot.define do
  factory :identifier_key, class: 'Spree::IdentifierKey' do
    sequence(:name) { |n| "key_#{n}" }
  end

  factory :identifier, class: 'Spree::Identifier' do
    sequence(:value) { |n| "extension-id-#{n}" }

    association :key, factory: :identifier_key
    association :attachable, factory: :user
  end
end
