# frozen_string_literal: true

json.partial! 'spree/api/shared/pagination', pagination: @identifier_keys
json.identifier_keys(@identifier_keys) do |identifier_key|
  json.partial!("spree/api/identifier_keys/identifier_key", identifier_key: identifier_key)
end
