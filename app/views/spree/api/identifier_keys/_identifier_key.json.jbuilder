# frozen_string_literal: true

json.cache! [I18n.locale, identifier_key] do
  json.call(identifier_key, *identifier_key_attributes)
end
