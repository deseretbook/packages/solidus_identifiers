# frozen_string_literal: true

json.partial!("spree/api/identifier_keys/identifier_key", identifier_key: @identifier_key)
