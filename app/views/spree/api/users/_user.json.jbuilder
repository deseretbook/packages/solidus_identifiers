# frozen_string_literal: true

json.call(user, *user_attributes)
json.bill_address do
  if user.bill_address
    json.partial!("spree/api/addresses/address", address: user.bill_address)
  else
    json.nil!
  end
end
json.ship_address do
  if user.ship_address
    json.partial!("spree/api/addresses/address", address: user.ship_address)
  else
    json.nil!
  end
end

# Custom code for Identifiers
if can?(:admin, user)
  json.identifiers user.identifiers do |identifier|
    json.partial!('spree/api/identifiers/identifier', identifier: identifier)
  end

  json.flattened_identifiers do
    user.identifiers.each do |identifier|
      json.set! identifier.key.name, identifier.value
    end
  end
end
# End Custom
