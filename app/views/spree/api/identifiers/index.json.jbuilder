# frozen_string_literal: true

json.partial! 'spree/api/shared/pagination', pagination: @identifiers
json.identifiers(@identifiers) do |identifier|
  json.partial!("spree/api/identifiers/identifier", identifier: identifier)
end
