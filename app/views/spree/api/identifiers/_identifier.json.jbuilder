# frozen_string_literal: true

json.cache! [I18n.locale, identifier] do
  json.call(identifier, *identifier_attributes)

  json.key do
    json.partial!('spree/api/identifier_keys/identifier_key', identifier_key: identifier.key)
  end

  json.attachable do
    json.id identifier.attachable_id
    json.type identifier.attachable_type
  end
end
