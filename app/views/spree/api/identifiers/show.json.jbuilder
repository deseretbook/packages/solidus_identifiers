# frozen_string_literal: true

json.partial!("spree/api/identifiers/identifier", identifier: @identifier)
