# frozen_string_literal: true

module Spree
  ##
  # A Key to for determining what the identifier represents.
  # (facebook, google, twitter)
  #
  class IdentifierKey < Spree::Base
    has_many :identifiers, class_name: 'Spree::Identifier', foreign_key: :key_id, dependent: :restrict_with_exception, inverse_of: :key

    validates :name, uniqueness: true
    validates :name, presence: true
  end
end
