# frozen_string_literal: true

module Spree
  ##
  # Identifier to store 3rd party or external identifiers for records
  # such as a user or product.
  #
  class Identifier < Spree::Base
    scope :with_key, ->(name) { joins(:key).where(spree_identifier_keys: { name: name }) }

    belongs_to :key, class_name: 'Spree::IdentifierKey', foreign_key: :key_id, inverse_of: :identifiers
    belongs_to :attachable, polymorphic: true

    validates :value, presence: true
    validates :key_id, presence: true
    validates :key_id, uniqueness: { scope: [:attachable_id, :attachable_type] }
  end
end
