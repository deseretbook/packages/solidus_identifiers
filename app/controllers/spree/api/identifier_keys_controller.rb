# frozen_string_literal: true

module Spree
  module Api
    class IdentifierKeysController < Spree::Api::ResourceController
      helper ::Spree::Core::Engine.routes.url_helpers

      private

      def permitted_identifier_key_attributes
        Spree::PermittedAttributes.identifier_key_attributes
      end
    end
  end
end
