# frozen_string_literal: true

module Spree
  module Api
    class IdentifiersController < Spree::Api::ResourceController
      helper ::Spree::Core::Engine.routes.url_helpers

      private

      def permitted_identifier_attributes
        Spree::PermittedAttributes.identifier_attributes
      end
    end
  end
end
