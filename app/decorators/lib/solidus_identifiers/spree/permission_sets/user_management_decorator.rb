# frozen_string_literal: true

module SolidusIdentifiers
  module Spree
    module PermissionSets
      module UserManagementDecorator
        def activate!
          super

          can :manage, ::Spree::Identifier, attachable_type: ::Spree.user_class.to_s
        end

        ::Spree::PermissionSets::DefaultCustomer.prepend(self)
      end
    end
  end
end
