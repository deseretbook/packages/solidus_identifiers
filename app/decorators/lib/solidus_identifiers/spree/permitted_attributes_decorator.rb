# frozen_string_literal: true

module SolidusIdentifiers
  module Spree
    module PermittedAttributesDecorator
      def self.prepended(base)
        base::ATTRIBUTES << :identifier_attributes
        base::ATTRIBUTES << :identifier_key_attributes
        base.mattr_reader :identifier_attributes, :identifier_key_attributes
      end

      @@identifier_attributes = %i[id value key_id attachable_id attachable_type]
      @@identifier_key_attributes = %i[id name]

      ::Spree::PermittedAttributes.prepend(self)
    end
  end
end
