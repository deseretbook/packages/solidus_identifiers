# frozen_string_literal: true

module SolidusIdentifiers
  module Spree
    module Api
      module ApiHelpersDecorator
        def self.prepended(base)
          base::ATTRIBUTES << :identifier_attributes
          base::ATTRIBUTES << :identifier_key_attributes
          base.mattr_reader :identifier_attributes, :identifier_key_attributes
        end

        @@identifier_attributes = %i[id value]
        @@identifier_key_attributes = %i[id name]

        if SolidusSupport.api_available?
          ::Spree::Api::ApiHelpers.prepend self
        end
      end
    end
  end
end
