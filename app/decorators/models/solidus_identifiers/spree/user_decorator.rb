# frozen_string_literal: true

module SolidusIdentifiers
  module Spree
    module UserDecorator
      def self.prepended(base)
        base.has_many :identifiers, as: :attachable
      end

      ::Spree.user_class.prepend(self)
    end
  end
end
