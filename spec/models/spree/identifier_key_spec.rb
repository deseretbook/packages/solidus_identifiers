# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Spree::IdentifierKey, type: :model do
  subject(:key) { instance }

  let(:instance) { FactoryBot.build(:identifier_key) }

  describe 'validations' do
    it { is_expected.to be_valid }

    it 'must be unique' do
      name = 'my-key'

      described_class.create!(name: name)
      expect { described_class.create!(name: name) }.
        to raise_error ActiveRecord::RecordInvalid
    end
  end
end
