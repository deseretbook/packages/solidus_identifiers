# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Spree::Identifier, type: :model do
  subject(:identifier) { instance }

  let(:instance) { FactoryBot.build(:identifier, key: key, attachable: user) }
  let(:user) { FactoryBot.create(:user) }
  let(:key) { FactoryBot.create(:identifier_key) }

  describe 'validations' do
    let(:missing_attr) { nil }

    before do
      instance.send("#{missing_attr}=", nil) if missing_attr
    end

    it { is_expected.to be_valid }

    context 'when value is blank' do
      let(:missing_attr) { :value }

      it { is_expected.not_to be_valid }
    end

    context 'when key_id is blank' do
      let(:missing_attr) { :key_id }

      it { is_expected.not_to be_valid }
    end
  end
end
