# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Spree::Api::IdentifiersController, type: :request do
  subject { response }

  # Setup
  let(:user) { create(:user) }

  let(:headers) do
    { 'ACCEPT': 'application/json' }
  end

  custom_authorization! do |_|
    can [:manage], ::Spree::Identifier
  end

  before do
    allow(Spree.user_class).to receive(:find_by).with(hash_including(:spree_api_key)) { user }
  end
  # End Setup

  # Spree::Api::IdentifiersController#index
  describe 'GET /api/identifiers' do
    let(:path) { '/api/identifiers' }

    before do
      FactoryBot.create_list(:identifier, 5)

      get path, headers: headers
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(:index)
      expect(response).to render_template('identifiers/_identifier')
      expect(response).to render_template('spree/api/shared/_pagination')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifiersController#show
  describe 'GET /api/identifiers/:id' do
    let(:identifier) { create(:identifier) }
    let(:path) { "/api/identifiers/#{identifier.to_param}" }

    before do
      get path, headers: headers
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(:show)
      expect(response).to render_template('identifiers/_identifier')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifiersController#new
  describe 'GET /api/identifiers/new' do
    let(:path) { '/api/identifiers/new' }

    before do
      get path, headers: headers
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(nil)
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifiersController#create
  describe 'POST /api/identifiers' do
    let(:path) { '/api/identifiers' }
    let(:identifier_key) { FactoryBot.create(:identifier_key) }
    let(:attachable) { FactoryBot.create(:user) }

    before do
      post path, headers: headers, params: {
        identifier: {
          value: 'test',
          key_id: identifier_key.id,
          attachable_id: attachable.id,
          attachable_type: attachable.class.to_s
        }
      }
    end

    it { is_expected.to have_http_status(:created) }

    it 'must render correct templates' do
      expect(response).to render_template(:show)
      expect(response).to render_template('identifiers/_identifier')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifiersController#update
  describe 'PUT /api/identifiers/:id' do
    let(:identifier) { create(:identifier) }
    let(:path) { "/api/identifiers/#{identifier.to_param}" }
    let(:identifier_key) { FactoryBot.create(:identifier_key) }
    let(:attachable) { FactoryBot.create(:user) }

    before do
      put path, headers: headers, params: {
        identifier: {
          value: 'test',
          key_id: identifier_key.id,
          attachable_id: attachable.id,
          attachable_type: attachable.class.to_s
        }
      }
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(:show)
      expect(response).to render_template('identifiers/_identifier')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifiersController#delete
  describe 'DELETE /api/identifiers' do
    let(:identifier) { create(:identifier ) }
    let(:path) { "/api/identifiers/#{identifier.to_param}" }

    before do
      delete path, headers: headers
    end

    it { is_expected.to have_http_status(:no_content) }

    it 'renders correct templates' do
      expect(response).to render_template(nil)
    end
  end
end
