# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Spree::Api::IdentifierKeysController, type: :request do
  subject { response }

  # Setup
  let(:user) { create(:user) }

  let(:headers) do
    { 'ACCEPT': 'application/json' }
  end

  custom_authorization! do |_|
    can [:manage], ::Spree::IdentifierKey
  end

  before do
    allow(Spree.user_class).to receive(:find_by).with(hash_including(:spree_api_key)) { user }
  end
  # End Setup

  # Spree::Api::IdentifierKeysController#index
  describe 'GET /api/identifier_keys' do
    let(:path) { '/api/identifier_keys' }

    before do
      FactoryBot.create_list(:identifier_key, 5)

      get path, headers: headers
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(:index)
      expect(response).to render_template('identifier_keys/_identifier_key')
      expect(response).to render_template('spree/api/shared/_pagination')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifierKeysController#show
  describe 'GET /api/identifier_keys/:id' do
    let(:identifier_key) { create(:identifier_key) }
    let(:path) { "/api/identifier_keys/#{identifier_key.to_param}" }

    before do
      get path, headers: headers
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(:show)
      expect(response).to render_template('identifier_keys/_identifier_key')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifierKeysController#new
  describe 'GET /api/identifier_keys/new' do
    let(:path) { '/api/identifier_keys/new' }

    before do
      get path, headers: headers
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(nil)
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifierKeysController#create
  describe 'POST /api/identifier_keys' do
    let(:path) { '/api/identifier_keys' }

    before do
      post path, headers: headers, params: {
        identifier_key: {
          name: 'test'
        }
      }
    end

    it { is_expected.to have_http_status(:created) }

    it 'must render correct templates' do
      expect(response).to render_template(:show)
      expect(response).to render_template('identifier_keys/_identifier_key')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifierKeysController#update
  describe 'PUT /api/identifier_keys/:id' do
    let(:identifier_key) { create(:identifier_key) }
    let(:path) { "/api/identifier_keys/#{identifier_key.to_param}" }

    before do
      put path, headers: headers, params: {
        identifier_key: {
          name: 'test'
        }
      }
    end

    it { is_expected.to have_http_status(:ok) }

    it 'must render correct templates' do
      expect(response).to render_template(:show)
      expect(response).to render_template('identifier_keys/_identifier_key')
    end

    it 'must respond with correct content type' do
      expect(response.content_type).to include 'application/json'
    end
  end

  # Spree::Api::IdentifierKeysController#delete
  describe 'DELETE /api/identifier_keys' do
    let(:identifier_key) { create(:identifier_key) }
    let(:path) { "/api/identifier_keys/#{identifier_key.to_param}" }

    before do
      delete path, headers: headers
    end

    it { is_expected.to have_http_status(:no_content) }

    it 'renders correct templates' do
      expect(response).to render_template(nil)
    end
  end
end
