# frozen_string_literal: true

require_relative 'lib/solidus_identifiers/version'

Gem::Specification.new do |spec|
  spec.name = 'solidus_identifiers'
  spec.version = SolidusIdentifiers::VERSION
  spec.authors = ['Taylor Scott']
  spec.email = 't.skukx@gmail.com'

  spec.summary = 'Map 3rd party services to a spree user'
  spec.description = 'Map 3rd party services to a spree user.' \
                     'These can include facebook, twitch, google, etc;'
  spec.homepage = 'https://gitlab.com/deseretbook/packages/solidus_identifiers'
  spec.license = 'BSD-3-Clause'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/deseretbook/packages/solidus_identifiers'

  spec.required_ruby_version = Gem::Requirement.new('~> 2.5')

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  files = Dir.chdir(__dir__) { `git ls-files -z`.split("\x0") }

  spec.files = files.grep_v(%r{^(test|spec|features)/})
  spec.test_files = files.grep(%r{^(test|spec|features)/})
  spec.bindir = "exe"
  spec.executables = files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'solidus_core', ['>= 2.0.0', '< 3']
  spec.add_dependency 'solidus_support', '>= 0.4.0'

  spec.add_development_dependency 'byebug'
  spec.add_development_dependency 'rails-controller-testing'
  spec.add_development_dependency 'solidus_dev_support'
end
