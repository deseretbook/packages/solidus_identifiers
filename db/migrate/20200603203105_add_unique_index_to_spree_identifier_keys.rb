# frozen_string_literal: true

class AddUniqueIndexToSpreeIdentifierKeys < ActiveRecord::Migration[5.2]
  def change
    add_index :spree_identifier_keys, :name, unique: true
  end
end
