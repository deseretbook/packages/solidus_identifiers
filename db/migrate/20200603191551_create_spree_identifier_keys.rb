# frozen_string_literal: true

class CreateSpreeIdentifierKeys < ActiveRecord::Migration[5.2]
  def change
    create_table :spree_identifier_keys do |t|
      t.string :name, unique: true, null: false
      t.timestamps
    end
  end
end
