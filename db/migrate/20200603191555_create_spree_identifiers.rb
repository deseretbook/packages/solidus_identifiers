# frozen_string_literal: true

class CreateSpreeIdentifiers < ActiveRecord::Migration[5.2]
  def change
    create_table :spree_identifiers do |t|
      t.string :value, null: false
      t.integer :key_id
      t.integer :attachable_id
      t.string :attachable_type
      t.timestamps
    end

    add_index :spree_identifiers, [:attachable_id, :attachable_type],
      name: 'ext_id_attachable_index'
    add_index :spree_identifiers, [:attachable_id, :attachable_type, :key_id],
      unique: true, name: 'ext_id_uniq_attachable_index'
  end
end
